<?php

namespace Palmers\EmailAttachment\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Palmers\EmailAttachment\Model\Email as Email;

class Import implements ObserverInterface
{
    protected $_email;

    /**
     * Import constructor.
     * @param Email $emailModel
     */
    public function __construct(
        Email $emailModel
    )
    {
        $this->_email = $emailModel;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $filename = $observer->getEvent()->getFilename();
        $reportData = $observer->getEvent()->getReportData();

        $splitted = explode('/', $filename);

        $this->_email->sendReportWithCsv($splitted[count($splitted)-1], $reportData);
    }
}