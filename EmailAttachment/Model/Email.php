<?php
namespace Palmers\EmailAttachment\Model;

use \Magento\Framework\Model\AbstractModel;


/**
 * ImportExport job model
 *
 */
class Email extends AbstractModel
{
    /**
     * CMS block cache tag
     */
    CONST PATH_SAP_IMPORT         = 'palmers-transfer/SAP/Import';
    CONST FIREBEAR_REPORT_FAILS_DIRECTORY = '/firebear/';
    CONST NOREPLY = 'noreply@palmers-shop.com';

    public $inlineTranslation;
    protected $_scopeConfig;
    protected $_transportBuilder;
    protected $_ioAdapter;
    protected $_directoryList;

    /**
     * Email constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Firebear\ImportExport\Model\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $ioAdapter
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Palmers\EmailAttachment\Model\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $ioAdapter
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_directoryList = $directoryList;
        $this->_ioAdapter = $ioAdapter;
    }

    /**
     * Send an email with CSV attachement
     *
     * @param $filename
     * @param $content
     */
    public function sendReportWithCsv($filename, $content)
    {
        $isAllowToSend = $this->_scopeConfig->getValue(
            'attachment/notification/enabled_import_notification',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $subject = $this->_scopeConfig->getValue(
            'attachment/notification/report_subject',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $description = $this->_scopeConfig->getValue(
            'attachment/notification/report_description',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $reportName = $this->_scopeConfig->getValue(
            'attachment/notification/report_name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $emailAddress = $this->_scopeConfig->getValue(
            'attachment/notification/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if (!$isAllowToSend) return;

        $post = [
            'name' => "{$filename} " . $description,
        ];

        /**
         * create attachment with error list
         */
        $reportFilePath = $this->_createSummaryAttachment("{$reportName}" . '.csv', $content);

        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($post);

        $sender = [
            'name' => $subject,
            'email' => Email::NOREPLY,
        ];

        $transport = $this->_transportBuilder
            ->setTemplateIdentifier('send_notification_email_template')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(['data' => $postObject])
            ->setFrom($sender)
            ->addTo("{$emailAddress}")
            ->addAttachment(file_get_contents($reportFilePath), "{$reportName}" . '.csv')
            ->getTransport();

        $transport->sendMessage();
    }

    /**
     * @param $fileName
     * @param $content
     * @return string
     */
    protected function _createSummaryAttachment($fileName, $content)
    {
        $reportPath = $this->_directoryList->getPath('media') . Email::FIREBEAR_REPORT_FAILS_DIRECTORY;
        if (!is_dir($reportPath)) {
            $this->_ioAdapter->mkdir($reportPath, 0775);
        }

        $this->_ioAdapter->open(array('path'=>$reportPath));
        $this->_ioAdapter->write($fileName, $content, 0666);

        return $reportPath . $fileName;
    }
}
