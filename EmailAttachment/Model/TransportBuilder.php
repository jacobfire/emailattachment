<?php
/**
 * Extending sending of emails
 */

namespace Palmers\EmailAttachment\Model;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    /**
     * @param $content
     * @param $filename
     * @return $this
     */
    public function addAttachment($content, $filename)
    {
        $this->message->createAttachment(
            $content,
            \Zend_Mime::TYPE_OCTETSTREAM,
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $filename
        );
        return $this;
    }
}